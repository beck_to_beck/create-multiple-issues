from api_calls import ApiCalls
import datetime
import time

class IssueCreator():
    def __init__(self, credentials, info):
        self.url = credentials['url']
        self.username = credentials['username']
        self.password = credentials['password']
        self.info = info
        self.project_id = info['pid']
        self.summary = info['summary']
        self.issue_type_id = info['issutypeId']
        self.rest = ApiCalls()
        self.rest.setAuth('basic', username=self.username,
                          password=self.password)
        print(credentials)
        print(info)

    def description(self):
        print(self.url, self.username, self.password)
        print(self.info)     

    def createElement(self, data):
        request_type = 'POST'
        URL = self.url+'/rest/api/2/issue'
        json_data = {
            "fields": {
                "project": {
                    'id': data['project_id']
                },
                'summary': data['summary'],
                'issuetype': {
                    'id': data['issue_type_id']
                }
            }
        }
        response = self.rest.request(request_type, URL, json_data=json_data)

        return response

    def createTestElement(self, sequence):
        data = {
            'project_id': self.project_id,
            'summary': self.summary + str(sequence),
            'issue_type_id': self.issue_type_id
        }
        return self.createElement(data)

    def testCreateMany(self, number):
        for element in range(0, number):
            response = self.createTestElement(element)
            self.logResponse(response, 'element creation')
            time.sleep(0.5)

    # def logText(self, text):
    #     now = datetime.datetime.now()
    #     text = '['+str(now)+'] '+text
    #     with open(self.log_file_name, 'a') as log_file:
    #         log_file.write(text+'\n')

    def logResponse(self, response, description):
        response_code = response.status_code

        if response.headers['Content-Type'].startswith('application/json'):
            response_text = response.text
        else:
            response_text = 'Response type is ' + \
                response.headers['Content-Type'] + \
                ' not showing here (only showing JSON)'

        success = 'was successful' if response_code < 300 else 'failed'

        # self.logText("Operation '"+description+"' "+success +
        #              ' with status '+str(response_code))
        # self.logText("Response was: ")
        # self.logText(response_text)



