import inquirer

class AskUser():

    def askQuestion(self, question, options):
        question = [
            inquirer.List('answer',
                          message = question,
                          choices = options),
        ]

        return inquirer.prompt(question)['answer']



