from library.api_calls import ApiCalls
import time

def main():
    # Authentication
    instance = 'https://<org>.atlassian.net'
    username = '<your e-mail to access Jira>'
    password = '<your API Token>'

    api_call = ApiCalls()
    api_call.setAuth('basic', username=username, password=password)

    project_id = '<project_id>'

    issues_info = [
        {
            'issue_id': 'issues_id_1',
            'number_of_issues': a,
            'summary': 'A'
        },
        {
            'issue_id': 'issues_id_2',
            'number_of_issues': b,
            'summary': 'B'
        },
        {
            'issue_id': 'issues_id_3',
            'number_of_issues': 4,
            'summary': 'C'
        }
    ]

    for issue in issues_info:
        for x in range(issue['number_of_issues']):
            json_data_create_issue = {
                "fields": {
                    "project": {
                        'id': project_id
                    },
                    'summary': issue['summary'] + ' ' + str(x + 1),
                    'issuetype': {
                        'id': issue['issue_id']
                    }
                }
            }
            
            reponse_create_issue = api_call.request(
                'POST', instance + '/rest/api/3/issue', json_data=json_data_create_issue)
            reponse_create_issue = reponse_create_issue.json()
            print(reponse_create_issue['key'])
    
if __name__ == '__main__':
    main()
